package ru.t1.chernysheva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.enumerated.Role;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_user", schema = "tm_test")
public final class UserDTO extends AbstractModelDTO {

    private static final long serialVersionUID = 1;

    @Basic
    @Column(name = "login")
    private String login;

    @Basic
    @Column(name = "first_name")
    private String firstName;

    @Basic
    @Column(name = "last_name")
    private String lastName;

    @Basic
    @Column(name = "middle_name")
    private String middleName;

    @Basic
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @Basic
    @Column(name = "locked")
    private Boolean locked = false;

    @Basic
    @Column(name = "password_hash")
    private String passwordHash;

    @Basic
    @Column(name = "email")
    private String email;

}
