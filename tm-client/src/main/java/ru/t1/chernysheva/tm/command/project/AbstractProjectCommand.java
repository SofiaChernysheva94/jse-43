package ru.t1.chernysheva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.dto.model.ProjectDTO;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + project.getCreated());
    }

    protected void renderProject(final List<ProjectDTO> projects) {
        projects.forEach(this::showProject);
    }

}
