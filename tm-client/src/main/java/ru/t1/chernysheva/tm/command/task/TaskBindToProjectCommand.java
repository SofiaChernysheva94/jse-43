package ru.t1.chernysheva.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-bind-to-project";

    @NotNull
    private final String DESCRIPTION = "Bind task to project.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();

        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);

        getTaskEndpoint().bindTaskToProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
